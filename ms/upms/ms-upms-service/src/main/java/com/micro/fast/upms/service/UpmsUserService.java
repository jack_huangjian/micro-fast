package com.micro.fast.upms.service;

import com.micro.fast.boot.starter.common.response.ServerResponse;
import com.micro.fast.common.service.SsmService;
import com.micro.fast.upms.pojo.UpmsUser;
import org.apache.avro.ipc.trace.ID;

/**
 * 权限管理系统用户服务接口
 * @author lsy
 */
public interface UpmsUserService<T,ID> extends SsmService<T,ID> {

  /**
   * 注册用户
   * @param upmsUser 用户对象
   * @param repeatPassword 确认密码
   * @return
   */
  ServerResponse<UpmsUser> register(UpmsUser upmsUser, String repeatPassword);

  /**
   * 检查用户名是否被占用
   * @param username 用户名
   * @return
   */
  ServerResponse checkUsername(String username);

  /**
   * 检查邮箱是否被占用
   * @param email
   * @return
   */
  ServerResponse checkEmail(String email);

  /**
   * 根据组织等信息查询用户
   */
  ServerResponse getUsersByOrgId(UpmsUser upmsUser, Integer orgId, int pageNum, int pageSize, String order_by);

  /**
   * 根据权限的类型获取所有的权限
   * @param userId 用户的id
   * @param type 权限的类型
   * @return
   */
  ServerResponse getAllPermissionByType(Integer userId,Integer type);


}
